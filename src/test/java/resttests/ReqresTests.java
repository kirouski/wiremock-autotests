package resttests;

import io.restassured.http.ContentType;
import models.User;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import java.util.List;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class ReqresTests {

    private final String baseUrl = "https://reqres.in/";

    @Test
    public void getUser() {
        List<User> userList = given()
                .baseUri(baseUrl)
                .basePath("api/users")
                .contentType(ContentType.JSON)
                .log().uri()
                .when()
                .get()
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().jsonPath().getList("data", User.class);

        assertFalse(userList.isEmpty());
    }
}
