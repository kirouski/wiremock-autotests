package runner;

import stubs.Stubs;

public class WireMockServerRunner {

    public static Stubs stubs = new Stubs();

    public static void main(String[] args) {
        stubs.setUp()
                .stubForCreateUser("UserCreateRequest", "UserCreateResponse")
               .stubGetUserLastName("UserGetResponse")
                .stubAnyBadRequest();
    }
}
