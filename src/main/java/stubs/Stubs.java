package stubs;

import com.github.tomakehurst.wiremock.WireMockServer;
import utils.JsonUtil;

import java.util.regex.Pattern;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class Stubs {

    public WireMockServer wireMockServer;
    private JsonUtil jsonUtil;

    public Stubs setUp() {
        wireMockServer = new WireMockServer(7778);
        wireMockServer.start();
        jsonUtil = new JsonUtil();
        return this;
    }

    public Stubs resetServer(){
        wireMockServer.resetAll();
        return this;
    }

    public Stubs stubForCreateUser(String requestFileNameString, String responseFileName){
        jsonUtil.setJSON("/__files/json/".concat(requestFileNameString));
        wireMockServer.stubFor(post("/api/user")
                .withHeader("Content-Type", equalToIgnoreCase("application/json"))
                        .withRequestBody(equalToJson(jsonUtil.getJSON().toString()))
                        .withRequestBody(matchingJsonPath("$.name", equalTo("Yury")))
                .willReturn(aResponse()
                        .withStatus(201)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("json/" + responseFileName)));
        return this;
    }


    public Stubs stubGetUserLastName(String responseFileName){
        wireMockServer.stubFor(get(urlPathEqualTo("/api/user"))
                .withQueryParam("name", equalTo("yury"))
                .withHeader("Content-Type", equalToIgnoreCase("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("json/" + responseFileName)));

        return this;
    }

    public Stubs stubAnyBadRequest(){
        wireMockServer.stubFor(any(anyUrl())
                .atPriority(10)
                .willReturn(aResponse()
                        .withStatus(404)
                        .withBody("{\"status\":\"Error\",\"message\":\"Endpoint not found\"}")));
        return this;
    }

}
